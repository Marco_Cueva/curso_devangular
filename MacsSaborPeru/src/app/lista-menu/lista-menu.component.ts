import { Component, OnInit } from '@angular/core';
import { Menu, OnInit } from './../models/menu.models';

@Component({
  selector: 'app-lista-menu',
  templateUrl: './lista-menu.component.html',
  styleUrls: ['./lista-menu.component.css']
})
export class ListaMenuComponent implements OnInit {

  listado: Menu[];

  constructor() {
    this.listado=[];
  }

  ngOnInit(): void {
  }

  agregar(nombre:string,precio:number,descripcion:string):boolean{
    this.listado.push(new Menu(nombre,precio,descripcion));
    console.log(this.listado);
    return false;
  }

}
