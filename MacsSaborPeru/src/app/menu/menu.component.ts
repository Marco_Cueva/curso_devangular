import { Component, OnInit,Input,HostBinding} from '@angular/core';
import { Menu, OnInit } from './../models/menu.models';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @Input() menu: Menu;
  @HostBinding('attr.class') cssClass='col-md-4';


  constructor() { }

  ngOnInit(): void {
  }

}
