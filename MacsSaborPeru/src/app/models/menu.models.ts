export class Menu{
  nombre:string;
  precio:number;
  descripcion:string;

  constructor(n:string,p:number,d:string){
    this.nombre=n;
    this.precio=p;
    this.descripcion=d;
  }
}
